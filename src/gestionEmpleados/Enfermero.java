
package gestionEmpleados;

import gestionEmpleados.Empleado;
import java.time.LocalDate;


public class Enfermero extends Empleado{
    private String rolAsignado;
    private Integer licensiaMedica;

    public Enfermero(String rolAsignado, Integer licensiaMedica, Integer numeroDeEmpleado, String telefono, LocalDate fechaDeContratacion, Double salario, String nombre, String apellido, String dni, String email, LocalDate fechaDeNacimiento) {
        super(numeroDeEmpleado, telefono, fechaDeContratacion, salario, nombre, apellido, dni, email, fechaDeNacimiento);
        this.rolAsignado = rolAsignado;
        this.licensiaMedica = licensiaMedica;
    }

    public Integer getLicensiaMedica() {
        return licensiaMedica;
    }
    
    
   

    public String getRolAsignado() {
        return rolAsignado;
    }

    public void setRolAsignado(String rolAsignado) {
        this.rolAsignado = rolAsignado;
    }

    @Override
    public void verDatos() {
        System.out.println("nombre: "+getNombre());
        System.out.println("nombre: "+getApellido());
        System.out.println("nombre: "+getSalario());
        System.out.println("nombre: "+getRolAsignado());
        System.out.println("licensia medica: "+getLicensiaMedica());
    }

    
   
    
}
