package gestionEmpleados;

import gestionAfiliados.Persona;
import java.time.LocalDate;

// esta clase sera abstracta 
public abstract class Empleado extends Persona implements Comparable<Empleado>{
    private Integer numeroDeEmpleado;
    private String telefono;
    private LocalDate fechaDeContratacion;
    private Double salario;
    private Boolean estado;

    public Empleado(Integer numeroDeEmpleado, String telefono, LocalDate fechaDeContratacion, Double salario, String nombre, String apellido, String dni, String email, LocalDate fechaDeNacimiento) {
        super(nombre, apellido, dni, email, fechaDeNacimiento);
        this.numeroDeEmpleado = numeroDeEmpleado;
        this.telefono = telefono;
        this.fechaDeContratacion = fechaDeContratacion;
        this.salario = salario;
        this.estado = true;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Double getSalario() {
        return salario;
    }
    

    public Integer getNumeroDeEmpleado() {
        return numeroDeEmpleado;
    }
    
    public String getTelefono() {
        return telefono;
    }
   
    public  LocalDate getFechaDeContratacion() {
        return fechaDeContratacion;
    }
    
    
    public void cambiarEstado() {
       if(this.getEstado()==true){
           this.setEstado(false);
       }else{
           this.setEstado(true);
       }
    }
    
    //metodo abstracto
    public abstract void verDatos();
    

  
    @Override
    public boolean equals(Object obj) {
        Empleado empleado=(Empleado) obj;
        return this.getNumeroDeEmpleado().equals( empleado.getNumeroDeEmpleado());
        
    }

    @Override
    public int compareTo(Empleado obj) {
        Empleado empleado=(Empleado) obj;
        
        return this.getNombre().compareTo(empleado.getNombre());
    }
    
    
    
    
}
