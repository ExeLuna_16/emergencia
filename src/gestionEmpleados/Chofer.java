
package gestionEmpleados;

import gestionMoviles.Moviles;
import java.time.LocalDate;


public class Chofer extends Empleado{    
    private int numeroDeLicencia;
    private Moviles unidad;

    public Chofer(int numeroDeLicencia, Integer numeroDeEmpleado, String telefono, LocalDate fechaDeContratacion, Double salario, String nombre, String apellido, String dni, String email, LocalDate fechaDeNacimiento) {
        super(numeroDeEmpleado, telefono, fechaDeContratacion, salario, nombre, apellido, dni, email, fechaDeNacimiento);
        this.numeroDeLicencia = numeroDeLicencia;
    }

    public Moviles getUnidad() {
        return unidad;
    }

    public void setUnidad(Moviles unidad) {
        this.unidad = unidad;
    }


    public int getNumeroDeLicencia() {
        return numeroDeLicencia;
    }

    public void setNumeroDeLicencia(int numeroDeLicencia) {
        this.numeroDeLicencia = numeroDeLicencia;
    }
    

    @Override
    public void verDatos() {
        System.out.println("nombre: "+getNombre());
        System.out.println("nombre: "+getApellido());
        System.out.println("nombre: "+getSalario());
        System.out.println("licensia de conducir: "+getNumeroDeLicencia());
    }
    
    
    
}
