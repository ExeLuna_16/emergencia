
package gestionEmpleados;

import java.time.LocalDate;


/**
 *
 * @author Usuario
 */
public class Medico extends Empleado{
    private String especialidad;
    private Integer licensiaMedica;

    public Medico(String especialidad, Integer licensiaMedica, Integer numeroDeEmpleado, String telefono, LocalDate fechaDeContratacion, Double salario, String nombre, String apellido, String dni, String email, LocalDate fechaDeNacimiento) {
        super(numeroDeEmpleado, telefono, fechaDeContratacion, salario, nombre, apellido, dni, email, fechaDeNacimiento);
        this.especialidad = especialidad;
        this.licensiaMedica = licensiaMedica;
    }
    
    public Integer getLicensiaMedica() {
        return licensiaMedica;
    }
    
    
    public String getEspecialidad() {
        return especialidad;
    }

    @Override
    public void verDatos() {
        System.out.println("nombre: "+getNombre());
        System.out.println("nombre: "+getApellido());
        System.out.println("nombre: "+getSalario());
        System.out.println("nombre: "+getEspecialidad());
        System.out.println("licensia medica: "+getLicensiaMedica());
    }

    

   
   
    
}
