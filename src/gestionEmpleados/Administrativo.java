
package gestionEmpleados;

import gestionEmpleados.Empleado;
import java.time.LocalDate;

/**
 *
 * @author Usuario
 */
public class Administrativo extends Empleado{
    private String rolAsignado;

    public Administrativo(String rolAsignado, Integer numeroDeEmpleado, String telefono, LocalDate fechaDeContratacion, Double salario, String nombre, String apellido, String dni, String email, LocalDate fechaDeNacimiento) {
        super(numeroDeEmpleado, telefono, fechaDeContratacion, salario, nombre, apellido, dni, email, fechaDeNacimiento);
        this.rolAsignado = rolAsignado;
    }

    
    
    
    public String getRolAsignado() {
        return rolAsignado;
    }

    public void setRolAsignado(String rolAsignado) {
        this.rolAsignado = rolAsignado;
    }
    

    @Override
    public void verDatos() {
        System.out.println("nombre: "+getNombre());
        System.out.println("nombre: "+getApellido());
        System.out.println("nombre: "+getSalario());
        System.out.println("nombre: "+getRolAsignado());
    }
    
}
