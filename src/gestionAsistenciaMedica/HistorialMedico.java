 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionAsistenciaMedica;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Ordenador
 */
public class HistorialMedico {
    
   private String numSolicitud;
   private String resultadoAtencion;
   private String tipoAtencion;
   private String diagnostico;

    public HistorialMedico(String numSolicitud, String resultadoAtencion, String tipoAtencion, String diagnostico) {
        this.numSolicitud = numSolicitud;
        this.resultadoAtencion = resultadoAtencion;
        this.tipoAtencion = tipoAtencion;
        this.diagnostico = diagnostico;
    }

    public void setNumSolicitud(String numSolicitud) {
        this.numSolicitud = numSolicitud;
    }

    public void setResultadoAtencion(String resultadoAtencion) {
        this.resultadoAtencion = resultadoAtencion;
    }

    public void setTipoAtencion(String tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }
    
    public String getNumSolicitud() {
        return numSolicitud;
    }

    public String getResultadoAtencion() {
        return resultadoAtencion;
    }

    public String getTipoAtencion() {
        return tipoAtencion;
    }

    public String getDiagnostico() {
        return diagnostico;
    }
   
   
    
}
