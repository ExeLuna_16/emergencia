package gestionAsistenciaMedica;

import emergencia.SistemaPrincipal;
import excepciones.ObjetoInexistenteException;
import excepciones.RegistroVacioException;
import gestionMoviles.Moviles;
import gestionAfiliados.Afiliado;
import gestionAfiliados.Persona;
import gestionDeRegistros.RegistroAfiliados;
import gestionDeRegistros.RegistroDeEmpleados;
import gestionDeRegistros.RegistroFacturas;
import gestionEmpleados.Chofer;
import gestionEmpleados.Enfermero;
import gestionEmpleados.Medico;
import gestionPago.Factura;
import gestionPago.Moratoria;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author Ordenador
 */
public class SolicitudEmergencia{
    
    private final RegistroDeEmpleados empleados = SistemaPrincipal.instancia().getRegistroEmplados();
    private final RegistroAfiliados afiliados = SistemaPrincipal.instancia().getRegistroAfiliados();
    
    private static int contador=0;
    private Integer numeSolicitud;
    private Persona paciente;
    private String descripcionProblema;
    private String especialidadMedico;
    private Medico medico;
    private Enfermero enfermero;
    private Moviles movil;
    private Chofer chofer;
    private Boolean atendida;
    private LocalDate fecha;
    
    
    //Constructor
    /*Se controla la mora y si es afiliado o no en el llamdo al metodo*/
    public SolicitudEmergencia(Persona paciente, String descripcionProblema,String especialidadMedico) throws ObjetoInexistenteException{
        
        this.numeSolicitud = ++contador;
        this.especialidadMedico = especialidadMedico;
        this.paciente = paciente;
        this.descripcionProblema = descripcionProblema;
        this.fecha = LocalDate.now();
        asignarPersonal(especialidadMedico);
        this.atendida = true;
    }
    
        //ASIGNACIONES
    public void asignarPersonal(String especialidad) throws ObjetoInexistenteException{
        Medico med = asignarMedico(especialidad);
        Chofer chofer = asignarChofer();
        Enfermero enfer = asignarEnfermero();
        setEnfermero(enfer);
        setMedico(med);
        setChofer(chofer);
    }
    
    public Chofer asignarChofer()throws ObjetoInexistenteException{
        return empleados.buscarChofer();
    }
    
    public Medico asignarMedico(String especialidad) throws ObjetoInexistenteException{
        return empleados.buscarMedico(especialidad);
    }
    
    public Enfermero asignarEnfermero()throws ObjetoInexistenteException{
        return empleados.buscarEnfermero();
    }
    
    public static int getContador() {
        return contador;
    }

    public static void setContador(int contador) {
        SolicitudEmergencia.contador = contador;
    }

    public Integer getNumeSolicitud() {
        return numeSolicitud;
    }

    public void setNumeSolicitud(Integer numeSolicitud) {
        this.numeSolicitud = numeSolicitud;
    }

    public Persona getPaciente() {
        return paciente;
    }

    public void setPaciente(Persona paciente) {
        this.paciente = paciente;
    }

    public String getDescripcionProblema() {
        return descripcionProblema;
    }

    public void setDescripcionProblema(String descripcionProblema) {
        this.descripcionProblema = descripcionProblema;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Enfermero getEnfermero() {
        return enfermero;
    }

    public void setEnfermero(Enfermero enfermero) {
        this.enfermero = enfermero;
    }
    
    public void setEspecialidadMedico(String especialidadMedico) {
        this.especialidadMedico = especialidadMedico;
    }

    public Moviles getMovil() {
        return movil;
    }

    public void setMovil(Moviles movil) {
        this.movil = movil;
    }

    public Chofer getChofer() {
        return chofer;
    }

    public void setChofer(Chofer chofer) {
        this.chofer = chofer;
    }
    
    public Boolean getAtendida() {
        return atendida;
    }

    public void setAtendida(Boolean atendida) {
        this.atendida = atendida;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    @Override
    public boolean equals(Object obj) {
        SolicitudEmergencia solicitud= (SolicitudEmergencia) obj;
        return this.getNumeSolicitud()==solicitud.getNumeSolicitud();
    }


   
   
}
