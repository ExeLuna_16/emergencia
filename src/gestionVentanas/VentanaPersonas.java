package gestionVentanas;

import emergencia.SistemaPrincipal;
import excepciones.ObjetoCargadoException;
import excepciones.RegistroVacioException;
import gestionAfiliados.Afiliado;
import gestionAfiliados.Familiar;
import gestionDeRegistros.RegistroAfiliados;
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.event.*;
import java.time.DateTimeException;
import java.time.LocalDate;



public class VentanaPersonas extends JFrame {
    JButton btnDarAlta; 
    /*Obtengo el registro de afiliados*/
    private final RegistroAfiliados registro = SistemaPrincipal.instancia().getRegistroAfiliados();
    /**
     * Creates new form VentanaPersonas
     */
    //CONSTRUCTOR
    public VentanaPersonas(String titulo, Integer opcion) {
        super(titulo);
        setSize(500,500);
        setLocationRelativeTo(null);
        btnDarAlta = new JButton("Dar Alta");
        btnDarAlta.setBounds(350,350,110,30);
        add(btnDarAlta);
        initComponents();
        ventanaDe(opcion);
        
        btnDarAlta.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                accionBoton(e,opcion);
            }
        });
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    public void ventanaDe(Integer opcion){
        if(opcion == 1){ //Datos Afiliado
            jPanelAfiliado.setVisible(true);
            jPanelFamiliar.setVisible(false);
        }
        if(opcion == 0){//Datos Familiar
            jPanelAfiliado.setVisible(false);
            jPanelFamiliar.setVisible(true);
        }
    }
    
    public void accionBoton(ActionEvent e, Integer opcion){
        try{
            //Datos Persona
            String nombre = txtNombre.getText().trim();
            String apellido = txtApellido.getText().trim();
            String dni = txtDni.getText().trim();
            String email = txtMail.getText().trim();
            Integer anio = Integer.parseInt(txtAnio.getText().trim());
            Integer mes = Integer.parseInt(txtMes.getText().trim());
            Integer dia = Integer.parseInt(txtDia.getText().trim());
            LocalDate fechaNacido = LocalDate.of(anio,mes,dia);
            btnObraSocial.add(obraSocialNo);
            btnObraSocial.add(obraSocialSi);
            /*Toma el resultado del boton como si fuera un multiplechoise
            y dependiendo la opcion se guarda*/
             
            if(nombre.isBlank()||apellido.isBlank()||dni.isBlank()||email.isBlank()){
                throw new NullPointerException("Elemento nulo");
            }
            if(opcion == 1){//crear Afiliado;
                String trabajo = txtTrabajo.getText().trim();
                Boolean obraSocial=false;
                if(obraSocialSi.isSelected()){
                    obraSocial = true;
                }else if(obraSocialNo.isSelected()){ 
                    obraSocial = false;
                }else if(trabajo.isBlank()){ 
                    throw new NullPointerException("Debe completar el campo trabajo");
                }
                Afiliado aux = new Afiliado(nombre,apellido,dni,email,fechaNacido,obraSocial,trabajo);
                grabarAfiliado(aux);
            }
            if(opcion == 0){ //Ventana Familiar
                String vinculo = txtVinculo.getText().trim();
                String dniAfiliado = txtDniAfiliado.getText().trim();
                if(vinculo.isBlank() || dniAfiliado.isBlank()){
                    throw new NullPointerException("Debe completar todos los campos");
                }
                if(registro.cantidadAfiliadosRegistrados() != 0){
                    
                    Afiliado duenio = registro.buscarAfiliado(dniAfiliado);
                    Familiar aux = new Familiar(nombre,apellido,dni,email,fechaNacido,vinculo);
                    grabarFamiliar(duenio,aux);
                }else{
                    throw new RegistroVacioException("no hay afiliados cargados");
                }
            }
        }catch(RegistroVacioException registroVacio){
            JOptionPane.showMessageDialog(null,"No hay afiliados cargados aun");
        }catch(DateTimeException dia){
            JOptionPane.showMessageDialog(null,"Por favor controle la fecha ingresada");
        }catch(NullPointerException nulo){
            JOptionPane.showMessageDialog(null,"Debe completar todos los campos");
        }catch(NumberFormatException num){
            JOptionPane.showMessageDialog(null,"Usted esta ingresando caracteres en lugar campos numericos");
        }    
    }
    
    public void grabarAfiliado(Afiliado nuevo){
        //Maneja las ecepciones que pueden surgir
        try{
            registro.darAlta(nuevo);
            JOptionPane.showMessageDialog(null,"Afiliado Cargado Correctamente");
        }catch(ObjetoCargadoException existe){
            JOptionPane.showMessageDialog(null,"El afiliado ingresado ya existe");
        }catch(NullPointerException nulo){
            JOptionPane.showMessageDialog(null,"No se llenaron todos los campos");
        }
    }
    
    public void grabarFamiliar(Afiliado duenioCuenta,Familiar nuevo){
        try{
            duenioCuenta.agregarFamiliar(nuevo);
            JOptionPane.showMessageDialog(null,"Familiar Cargado Correctamente");
        }catch(ObjetoCargadoException existe){
            JOptionPane.showMessageDialog(null,"El Familiar ingresado ya existe");
        }catch(NullPointerException nulo){
            JOptionPane.showMessageDialog(null,"No se llenaron todos los campos del familiar");
        }
        
    } 
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnObraSocial = new javax.swing.ButtonGroup();
        contenedor = new javax.swing.JPanel();
        etqNombre = new javax.swing.JLabel();
        etqApellido = new javax.swing.JLabel();
        etqDni = new javax.swing.JLabel();
        etqEmail = new javax.swing.JLabel();
        etqFechaNacimiento = new javax.swing.JLabel();
        etqAnio = new javax.swing.JLabel();
        etqMes = new javax.swing.JLabel();
        etqDia = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();
        txtDni = new javax.swing.JTextField();
        txtMail = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtAnio = new javax.swing.JTextField();
        txtMes = new javax.swing.JTextField();
        txtDia = new javax.swing.JTextField();
        jPanelFamiliar = new javax.swing.JPanel();
        eqtDNIDuenioCuenta = new javax.swing.JLabel();
        txtDniAfiliado = new javax.swing.JTextField();
        eqtVinculoFamiliar = new javax.swing.JLabel();
        txtVinculo = new javax.swing.JTextField();
        jPanelAfiliado = new javax.swing.JPanel();
        etqTrabajo = new javax.swing.JLabel();
        txtTrabajo = new javax.swing.JTextField();
        etqObraSocial = new javax.swing.JLabel();
        obraSocialSi = new javax.swing.JRadioButton();
        obraSocialNo = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        contenedor.setBackground(new java.awt.Color(255, 255, 255));

        etqNombre.setBackground(new java.awt.Color(255, 255, 255));
        etqNombre.setForeground(new java.awt.Color(0, 0, 0));
        etqNombre.setText("Nombre:");

        etqApellido.setBackground(new java.awt.Color(255, 255, 255));
        etqApellido.setForeground(new java.awt.Color(0, 0, 0));
        etqApellido.setText("Apellido:");

        etqDni.setBackground(new java.awt.Color(255, 255, 255));
        etqDni.setForeground(new java.awt.Color(0, 0, 0));
        etqDni.setText("DNI:");

        etqEmail.setBackground(new java.awt.Color(255, 255, 255));
        etqEmail.setForeground(new java.awt.Color(0, 0, 0));
        etqEmail.setText("Email:");

        etqFechaNacimiento.setBackground(new java.awt.Color(255, 255, 255));
        etqFechaNacimiento.setForeground(new java.awt.Color(0, 0, 0));
        etqFechaNacimiento.setText("Fecha Nacimiento:");

        etqAnio.setBackground(new java.awt.Color(255, 255, 255));
        etqAnio.setForeground(new java.awt.Color(0, 0, 0));
        etqAnio.setText("Año:");

        etqMes.setBackground(new java.awt.Color(255, 255, 255));
        etqMes.setForeground(new java.awt.Color(0, 0, 0));
        etqMes.setText("Mes");

        etqDia.setBackground(new java.awt.Color(255, 255, 255));
        etqDia.setForeground(new java.awt.Color(0, 0, 0));
        etqDia.setText("Dia");

        txtApellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidoActionPerformed(evt);
            }
        });

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });

        txtAnio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAnioActionPerformed(evt);
            }
        });

        txtMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMesActionPerformed(evt);
            }
        });

        txtDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiaActionPerformed(evt);
            }
        });

        eqtDNIDuenioCuenta.setForeground(new java.awt.Color(0, 0, 0));
        eqtDNIDuenioCuenta.setText("DNI Afiliado:");

        txtDniAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDniAfiliadoActionPerformed(evt);
            }
        });

        eqtVinculoFamiliar.setForeground(new java.awt.Color(0, 0, 0));
        eqtVinculoFamiliar.setText("Vinculo:");

        txtVinculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtVinculoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelFamiliarLayout = new javax.swing.GroupLayout(jPanelFamiliar);
        jPanelFamiliar.setLayout(jPanelFamiliarLayout);
        jPanelFamiliarLayout.setHorizontalGroup(
            jPanelFamiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFamiliarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelFamiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(eqtDNIDuenioCuenta)
                    .addComponent(eqtVinculoFamiliar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelFamiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtVinculo)
                    .addComponent(txtDniAfiliado, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelFamiliarLayout.setVerticalGroup(
            jPanelFamiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFamiliarLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanelFamiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(eqtVinculoFamiliar)
                    .addComponent(txtVinculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(jPanelFamiliarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(eqtDNIDuenioCuenta)
                    .addComponent(txtDniAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        etqTrabajo.setForeground(new java.awt.Color(0, 0, 0));
        etqTrabajo.setText("Trabajo:");

        etqObraSocial.setForeground(new java.awt.Color(0, 0, 0));
        etqObraSocial.setText("Obra Social:");

        btnObraSocial.add(obraSocialSi);
        obraSocialSi.setForeground(new java.awt.Color(0, 0, 0));
        obraSocialSi.setText("Si");
        obraSocialSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                obraSocialSiActionPerformed(evt);
            }
        });

        btnObraSocial.add(obraSocialNo);
        obraSocialNo.setForeground(new java.awt.Color(0, 0, 0));
        obraSocialNo.setText("No");

        javax.swing.GroupLayout jPanelAfiliadoLayout = new javax.swing.GroupLayout(jPanelAfiliado);
        jPanelAfiliado.setLayout(jPanelAfiliadoLayout);
        jPanelAfiliadoLayout.setHorizontalGroup(
            jPanelAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAfiliadoLayout.createSequentialGroup()
                .addGroup(jPanelAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAfiliadoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(etqTrabajo)
                        .addGap(18, 18, 18)
                        .addComponent(txtTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelAfiliadoLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(etqObraSocial))
                    .addGroup(jPanelAfiliadoLayout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(obraSocialSi)
                        .addGap(27, 27, 27)
                        .addComponent(obraSocialNo)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanelAfiliadoLayout.setVerticalGroup(
            jPanelAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAfiliadoLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanelAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etqTrabajo)
                    .addComponent(txtTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etqObraSocial)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelAfiliadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(obraSocialSi)
                    .addComponent(obraSocialNo))
                .addContainerGap(11, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout contenedorLayout = new javax.swing.GroupLayout(contenedor);
        contenedor.setLayout(contenedorLayout);
        contenedorLayout.setHorizontalGroup(
            contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(contenedorLayout.createSequentialGroup()
                            .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(etqDni)
                                .addComponent(etqEmail))
                            .addGap(19, 19, 19)
                            .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtMail, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                .addComponent(txtDni)))
                        .addGroup(contenedorLayout.createSequentialGroup()
                            .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(etqNombre)
                                .addComponent(etqApellido))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                .addComponent(txtApellido))))
                    .addGroup(contenedorLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(etqMes)
                            .addComponent(etqAnio)
                            .addComponent(etqDia))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDia, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtMes, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                                .addComponent(txtAnio, javax.swing.GroupLayout.Alignment.LEADING))))
                    .addComponent(etqFechaNacimiento))
                .addGap(30, 30, 30)
                .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelFamiliar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelAfiliado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        contenedorLayout.setVerticalGroup(
            contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(contenedorLayout.createSequentialGroup()
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etqNombre)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etqApellido)
                            .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etqDni)
                            .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(etqEmail)
                            .addComponent(txtMail, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(etqFechaNacimiento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etqAnio)
                            .addComponent(txtAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etqMes)
                            .addComponent(txtMes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(contenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etqDia)
                            .addComponent(txtDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(contenedorLayout.createSequentialGroup()
                        .addComponent(jPanelAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addComponent(jPanelFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(126, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtApellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidoActionPerformed

    private void txtAnioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAnioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAnioActionPerformed

    private void txtDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDiaActionPerformed

    private void txtMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMesActionPerformed

    private void txtVinculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtVinculoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVinculoActionPerformed

    private void obraSocialSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_obraSocialSiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_obraSocialSiActionPerformed

    private void txtDniAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDniAfiliadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDniAfiliadoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnObraSocial;
    private javax.swing.JPanel contenedor;
    private javax.swing.JLabel eqtDNIDuenioCuenta;
    private javax.swing.JLabel eqtVinculoFamiliar;
    private javax.swing.JLabel etqAnio;
    private javax.swing.JLabel etqApellido;
    private javax.swing.JLabel etqDia;
    private javax.swing.JLabel etqDni;
    private javax.swing.JLabel etqEmail;
    private javax.swing.JLabel etqFechaNacimiento;
    private javax.swing.JLabel etqMes;
    private javax.swing.JLabel etqNombre;
    private javax.swing.JLabel etqObraSocial;
    private javax.swing.JLabel etqTrabajo;
    private javax.swing.JPanel jPanelAfiliado;
    private javax.swing.JPanel jPanelFamiliar;
    private javax.swing.JRadioButton obraSocialNo;
    private javax.swing.JRadioButton obraSocialSi;
    private javax.swing.JTextField txtAnio;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtDia;
    private javax.swing.JTextField txtDni;
    private javax.swing.JTextField txtDniAfiliado;
    private javax.swing.JTextField txtMail;
    private javax.swing.JTextField txtMes;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtTrabajo;
    private javax.swing.JTextField txtVinculo;
    // End of variables declaration//GEN-END:variables
}
