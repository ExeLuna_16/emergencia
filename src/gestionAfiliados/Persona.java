package gestionAfiliados;

import java.time.LocalDate;

/** PUEDE LLEGAR A SER UNA CLASE ABSTRACTA YA QUE MAS DE UNA HEREDA DE ESTA
 *  su metodo abstracto puede ser solicitar asistencia medica
 * @author ExeLuna__
 */
public class Persona {
    //Atributos
    private String nombre;
    private String apellido;
    private String dni;
    private String email;
    private LocalDate fechaDeNacimiento;

    //Constructor
    public Persona(String nombre, String apellido, String dni, String email, LocalDate fechaDeNacimiento){
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.email = email;
        this.fechaDeNacimiento = fechaDeNacimiento;
    }
    
    //Setters y Getters 
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public void setApellido(String apellido){
        this.apellido = apellido;
    }
    
    public void setDni(String dni){
        this.dni = dni;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public void setFechaDeNacimiento(LocalDate fechaDeNacimiento){
        this.fechaDeNacimiento = fechaDeNacimiento;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public String getApellido(){
        return apellido;
    }
    
    public String getDni(){
        return dni;
    }
    
    public String getEmail(){
        return email;
    }
    
    public LocalDate getFechaNacimiento(){
        return fechaDeNacimiento;
    }
    
    //Redefinicion del metodo equals
    /*Dos personas van a ser iguales si su DNI es el mismo*/
    @Override
    public boolean equals(Object objeto){
        if(this == objeto){
            return true; //Objeto actual
        }
        if(objeto == null){
            return false; //objeto nulo o de otra clase distinta
        }
        Persona aux = (Persona) objeto;
        return this.getDni().equals(aux.getDni());
    }
    
}
