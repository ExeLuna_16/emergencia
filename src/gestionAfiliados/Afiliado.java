package gestionAfiliados;

import excepciones.ObjetoCargadoException;
import gestionAsistenciaMedica.HistorialMedico;
import gestionPago.Factura;
import gestionPago.Moratoria;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ExeLuna__
 */
public class Afiliado extends Persona{
    //Atributos
    private LocalDate fechaAfiliacion;
    private Boolean obraSocial;
    private String trabajo;
    private List<Familiar> familiares;
    private List<Factura> facturas;
    private List<HistorialMedico> historial;
    private Boolean tieneServicio;
    
    //Constructor
    /*Al registrar un afiliado automaticamente decimos que tiene servicio
    pero esto puede cambiar dependiendo del estado de mora, de modo que si se enccuentra
    en mora no podra solicitar asistencia medica */
    public Afiliado(String nombre, String apellido,String dni, String email, LocalDate fechaDeNacimiento,
            Boolean obraSocial, String trabajo){
        super(nombre,apellido, dni, email, fechaDeNacimiento);
        this.obraSocial = obraSocial;
        this.trabajo = trabajo;
        this.fechaAfiliacion = LocalDate.now();
        this.tieneServicio = true;
        this.familiares = new ArrayList<Familiar>();
        this.facturas = new ArrayList<Factura>();
        this.historial=new ArrayList<HistorialMedico>();
    }
    
    /*Agrega familiar*/
    public void agregarFamiliar(Familiar nuevo) throws ObjetoCargadoException{
        if(nuevo == null){
            throw new NullPointerException("El objeto es nulo");
        }
        Familiar controlador = buscarFamiliar(nuevo.getDni());
        if(controlador == null){
            familiares.add(nuevo);
        }else{
            throw new ObjetoCargadoException("El familiar ya existe en el registro");
        }
    }
    
    public void agregarFactura(Factura factura){
        facturas.add(factura);
    }
    public Integer cantidadFacturas(){
        return facturas.size();
    }
    public void agregarDatosAlHistorial(HistorialMedico info){
        historial.add(info);
    }
    
    /*Metodo que devuelve la cantidad de familiares*/
    public Integer cantidadFamiliaresAsociados(){
        return familiares.size();
    }
    
    public Factura ultimaFactura(){
        Factura aux = facturas.get(cantidadFacturas()-1);
        return aux;
    }
    //Metodo que devuelve un familiar 
    /*El criterio de este metodo es que el Afiliado tenga control
    sobre sus familiares agregados y sea quien pase sus datos*/
    public Familiar buscarFamiliar(String dniFamiliar){
        int i = 0;
        while(i < cantidadFamiliaresAsociados() && !familiares.get(i).getDni().equals(dniFamiliar)){
            i++;
        }
        if(i < cantidadFamiliaresAsociados()){
            return familiares.get(i);
        }
        return null;
    }
    
    /*Eliminar familiar*/
    public void eliminarFamiliar(String dniFamiliar){
        Familiar borrar = buscarFamiliar(dniFamiliar);
        if(borrar == null){
            throw new NullPointerException("El familiar no esta registrado");
        }else{
            familiares.remove(borrar);
        }
    }
    
    /*Modificar familiar*/
    public void modificarFamiliar(String dniAModificar, Familiar nuevo){
       eliminarFamiliar(dniAModificar);
       try{
           agregarFamiliar(nuevo);
       }catch(ObjetoCargadoException existe){
           System.out.println("Error al modificar el afiliado");
       }
    }
    
    //Setters y Getters
 
    public Boolean getObraSocial() {
        return obraSocial;
    }
   
    public void setObraSocial(Boolean obraSocial) {
        this.obraSocial = obraSocial;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }
  
    public void setTieneServicio(Boolean cambio){
        this.tieneServicio = cambio;
    }
    public Boolean getTieneServicio(){
        return tieneServicio;
    }
    
    public void solicitarAtencionMedica(){
        if(tieneServicio){
            System.out.println("Solicitud de atención enviada");
        }else{
            System.out.println("Usted se encuentra en mora por lo tanto no tiene servicio");
        }   
    }
    
}
