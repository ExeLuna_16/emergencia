package gestionAfiliados;

import gestionAsistenciaMedica.HistorialMedico;
import java.time.LocalDate;

/**
 *
 * @author ExeLuna__
 */

public class Familiar extends Persona{
    //Atributos
    private String vinculoAfiliado;//Acá va una relacion a afiliado, por ahora es un String que indica el nombre del afiliado titular
    private HistorialMedico historial;
    //Constructor
    public Familiar(String nombre, String apellido, String dni, String email, LocalDate fechaNacimiento, String vinculoAfiliado){
        super(nombre,apellido,dni,email,fechaNacimiento);
        this.vinculoAfiliado = vinculoAfiliado.toUpperCase(); //Se pasa a mayusculas
    }
    
    //Setters y Getterss
    public String getVinculoAfiliado() {
        return vinculoAfiliado;
    }

    public void setVinculoAfiliado(String vinculoAfiliado) {
        this.vinculoAfiliado = vinculoAfiliado;
    }
    
    public HistorialMedico getHistorial() {
        return historial;
    }

    public void setHistorial(HistorialMedico historial) {
        this.historial = historial;
    }
    
    
}
