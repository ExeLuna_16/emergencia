/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionDeRegistros;

import excepciones.ObjetoInexistenteException;
import gestionAsistenciaMedica.SolicitudEmergencia;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ordenador
 */
public class RegistroSolicitudDeEmergencia {
    private List<SolicitudEmergencia> solicitudes;

    public RegistroSolicitudDeEmergencia() {
        this.solicitudes = new ArrayList<>();
    }
    
    public void agregarSolicitud(SolicitudEmergencia solicitud){
        if (solicitud == null) {
            throw new IllegalArgumentException("El valor ingresado no puede ser nulo");
        }
        if (solicitudes.contains(solicitud)) {
            throw new IllegalArgumentException("Los datos de la solicitud ya fueron ingresados al registro");
        }

        solicitudes.add(solicitud);
    }
    
    public void eliminarSolicitudes(SolicitudEmergencia solicitud){
        if(!solicitudes.contains(solicitud)){
            throw new IllegalArgumentException("La solicitud no se encuentra en el registro");
        }
        solicitudes.remove(solicitud);
    }
    
    public SolicitudEmergencia buscarSolicitud(int numSolicitud) throws ObjetoInexistenteException{
       for(SolicitudEmergencia solicitud: solicitudes){
          if(numSolicitud== solicitud.getNumeSolicitud()){
              return solicitud;
          }
       }
        throw new ObjetoInexistenteException("Solicitud no encontrada");
   } 
}
