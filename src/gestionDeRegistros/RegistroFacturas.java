package gestionDeRegistros;

import java.util.List;
import java.util.ArrayList;
import java.time.LocalDate;
import gestionAfiliados.Afiliado;
import gestionPago.Factura;

/**
 *
 * @author ExeLuna__
 */
public class RegistroFacturas {
    //Atributos
    private List<Factura> registro;
    
    //Constructor
    public RegistroFacturas(){
        this.registro = new ArrayList<Factura>();
    }
    
    /*Agregar Factura*/
    public void agregarFactura(Factura nueva){
        Factura aux = buscarFactura(nueva.getNumeroFactura());
        if(aux != null){
            throw new RuntimeException("La factura existe en el registro");
        }else{
            registro.add(nueva);
        }
    }
    
    /*Buscar factura
    busca por numero de factura*/
    public Factura buscarFactura(Integer buscada){
        int i = 0;
        while(i < registro.size() && !registro.get(i).equals(buscada)){
            i++;
        }
        if(i < registro.size()){
            return registro.get(i);
        }
        return null;
    }
    
    /*Eliminar Facturas
    hay que tener cuidado con este metodo porque legalmente el eliminar una factura
    implica una nota de credito, consultar con el profesor*/
    public void eliminarFactura(Integer buscada){
        Factura temporal = buscarFactura(buscada);
        if(temporal == null){
          throw new RuntimeException("item no encontrado"); 
        }else{
            registro.remove(temporal);
        }
    }
    
    /*Modificar factura
    consultar si esto es posible*/
    public void modificarFactura(Integer numeroFactura,Factura nueva){
        eliminarFactura(numeroFactura);
        agregarFactura(nueva);
    }
    
    /*Buscar facturas de cliente por intervalo de tiempo
    Ese if tan largo basicamente compara que las facturas sean del cliente buscado y tambien que 
    esten en el rango de fechas buscadas incluyendo a las fechas de inicio y fecha final que son parametros de la busqueda
    lo hace como si fuera:
    fechaInicio <= fechaEmisionDeFactura y fechaFin >= fechaEmisionDeFactura*/
    public List<Factura> historialFacturasDeCliente(LocalDate fechaInicio,LocalDate fechaFin,Afiliado cliente){
        List<Factura> historial = new ArrayList<Factura>();
        for(Factura indice:registro){
            if(indice.getClienteAfiliado().equals(cliente) && (fechaInicio.isBefore(indice.getFechaEmision()) || fechaInicio.equals(indice.getFechaEmision())) && (fechaFin.isAfter(indice.getFechaEmision()) || fechaFin.equals(indice.getFechaEmision()))){
                /*guarda la factura que cumple la condicion*/
                historial.add(indice);
            }
        }
        return historial;
    }
    
    public Integer cantidadFacturas(){
        return registro.size();
    }
    
    public Factura obtenerFactura(Integer indice){
        return registro.get(indice);
    }
}
