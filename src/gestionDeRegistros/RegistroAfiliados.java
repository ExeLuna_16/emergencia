package gestionDeRegistros;

import excepciones.ObjetoCargadoException;
import excepciones.ObjetoInexistenteException;
import excepciones.RegistroVacioException;
import java.util.List;
import java.util.ArrayList;
import gestionAfiliados.Afiliado;
import gestionAfiliados.Familiar;
import gestionAsistenciaMedica.SolicitudEmergencia;
/**
 *
 * @author Windows 10
 */
public class RegistroAfiliados {
    //Atributos
    private List<Afiliado> registro;
    
    //Constructor
    public RegistroAfiliados(){
        this.registro = new ArrayList<Afiliado>();
    }

    public List<Afiliado> getRegistro() {
        return registro;
    }
    
    //Metodo para agregar afiliado
/*    public void agregarAfiliado(Afiliado afiliado){
        if (afiliado == null) {
            throw new IllegalArgumentException("El valor ingresado no puede ser nulo");
        }
        if (registro.contains(afiliado)) {
            throw new IllegalArgumentException("Los datos de la solicitud ya fueron ingresados al registro");
        }

        registro.add(afiliado);
    }*/
    
    /*Comprobar que no este agregado ya*/
    public void darAlta(Afiliado afiliado) throws ObjetoCargadoException{
        if(afiliado == null){
            throw new NullPointerException("El afiliado es nulo");
        }
        Afiliado controlador = buscarAfiliado(afiliado.getDni());
        if(controlador == null){
            registro.add(afiliado);
        }else{
            throw new ObjetoCargadoException("El afiliado ya esta registrado");
        }
    }
    /*Modificar afiliado*/
    public void modificarDatosAfiliado(String dniAModificar,Afiliado nuevo) throws ObjetoCargadoException,RegistroVacioException{
        efectuarBajaAfiliado(dniAModificar);
        darAlta(nuevo);
    }
    
    //Metodo para eliminar un afiliado
    /*Los criterios para eliminar son ajenos al metodo
    pero vamos a comprobar que el objeto a eliminar no sea nulo y que 
    exista en el registro*/
    public void efectuarBajaAfiliado(String dniDarBaja) throws RegistroVacioException{
        Afiliado borrar = buscarAfiliado(dniDarBaja);
        if(borrar == null){
            throw new NullPointerException("El dni buscado no pertenece a ningun afiliado registrado");
        }else{
            registro.remove(borrar);
        }
    }
    
    //Metodo util para llevar un control de cuantos afiliados hay en el registro
    public Integer cantidadAfiliadosRegistrados(){
        return registro.size();
    }
    
    //Metodo para buscar un afiliado
    /*Se usara un ciclo while ya que terminará cuando encuentre al objeto
    en el peor caso va a comparar hasta el final del registro
    y si no encuentra el objeto lanza una excepcion en tiempo de ejecucion
    (RuntimeException)*/
    public Afiliado buscarAfiliado(String dniBuscado){
        int i = 0;
           
        while(i < cantidadAfiliadosRegistrados() && !registro.get(i).getDni().equals(dniBuscado)){
            i++;
        }
        if(i < cantidadAfiliadosRegistrados()){ //Se encontro el elemento
            return registro.get(i);
        }
        return null;
    }
    
    //Metodo que va a devolver un familiar asosicado
    /*El acceso al familiar es mediante el afiliado*/
    public Familiar obtenerFamiliarRegistrado(String dniAfiliado, String dniFamiliar) throws ObjetoInexistenteException,RegistroVacioException{
        Afiliado auxiliar = buscarAfiliado(dniAfiliado);
        if(auxiliar == null){
            throw new NullPointerException("no existe afiliado con el dni ingresado");
        }
        if(auxiliar.cantidadFamiliaresAsociados() == 0){
            throw new RegistroVacioException("El afiliado buscado no cuenta con familiares registrados");
        }
        Familiar encontrado = auxiliar.buscarFamiliar(dniFamiliar);
        if(encontrado != null){
            return encontrado;
        }else{
            throw new NullPointerException("El dni ingresado no pertenece a un familiar registrado por el afiliado " + auxiliar.getNombre());
        }
    }
}
 