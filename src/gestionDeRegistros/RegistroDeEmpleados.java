
package gestionDeRegistros;

import excepciones.ObjetoInexistenteException;
import excepciones.ObjetoNuloException;
import gestionEmpleados.Chofer;
import gestionEmpleados.Empleado;
import gestionEmpleados.Enfermero;
import gestionEmpleados.Medico;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;




public class RegistroDeEmpleados {
    Set<Empleado> listaEmpleados;
    
    public RegistroDeEmpleados() {
        listaEmpleados=new HashSet<>();
        
    }

    public Set<Empleado> getListaEmpleados() {
        return listaEmpleados;
    }
    
    public void añadirEmpleado(Empleado empleado) throws ObjetoNuloException{
        if(empleado!=null){
            listaEmpleados.add(empleado);
        }else{
            throw new ObjetoNuloException("el objeto que se intenta añadir es nulo");
        }      
    }
    
    public void quitarEmpleado(Integer numero)throws ObjetoInexistenteException{
        int bandera=0;
        for(Empleado empleado: listaEmpleados){
            if(empleado.getNumeroDeEmpleado().equals(numero)){
                listaEmpleados.remove(empleado);
                System.out.println("se elimino empleado");
                bandera=1;
            }
        }
        if(bandera==0){
            throw new ObjetoInexistenteException("el empleado buscado no existe");
        }
        
    }
    
    public Empleado buscarEmpleado(Integer numero) throws ObjetoInexistenteException{
        
        for(Empleado empleado: listaEmpleados){
            if(empleado.getNumeroDeEmpleado().equals(numero)){
                return empleado;
            }
        }
        throw new ObjetoInexistenteException("el empleado buscado no existe");
        
    }
    
    //ordeno la lista por orden natural(nombre) y retorno la lista ya ordenada
    public List<Empleado> ordenarEmpleadosNombre(){
        List<Empleado> listaOrdenada=new ArrayList<>(listaEmpleados);//collection.sort no me deja usar una lista tipo Set
        Collections.sort(listaOrdenada);
        return listaOrdenada;
    }

    public Medico buscarMedico(String especialidad) throws ObjetoInexistenteException{
        for(Empleado empleado : listaEmpleados ){
            if(empleado instanceof Medico){  
                Medico medico=(Medico) empleado;
                if(medico.getEspecialidad().equals(especialidad) && medico.getEstado()==true){
                    return medico;
                }
            }
            
        }      
        throw new ObjetoInexistenteException("no hay medicos disponibles");
        
    }
    
    public Enfermero buscarEnfermero() throws ObjetoInexistenteException{
        for(Empleado empleado: listaEmpleados){
            if(empleado instanceof Enfermero){
                Enfermero enfermero=(Enfermero) empleado;
                if(enfermero.getEstado()==true){
                    return enfermero;
                }
                
            }
        }
        throw new ObjetoInexistenteException("no hay enfermeros disponibles");
    }
    
    public Chofer buscarChofer() throws ObjetoInexistenteException{
        for(Empleado empleado: listaEmpleados){
            if(empleado instanceof Chofer ){
                Chofer chofer=(Chofer) empleado;
                if(chofer.getEstado()==true){
                    return chofer;
                }
                
            }
        }
        throw new ObjetoInexistenteException("no hay choferes disponibles");
    }
   
    
    
}
