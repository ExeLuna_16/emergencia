/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionMoviles;

import gestionEmpleados.Chofer;

/**
 *
 * @author Ordenador
 */
public class Moviles{
    
    private String marca;
    private String modelo;
    private String matricula;
    private Chofer chofer;

    public Moviles(String marca, String modelo, String matricula,Chofer chofer) {
        this.marca = marca;
        this.modelo = modelo;
        this.matricula = matricula;
        this.chofer = chofer;
    }

    public Chofer getChofer() {
        return chofer;
    }

    public void setChofer(Chofer chofer) {
        this.chofer = chofer;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    @Override
    public String toString() {
        return "Moviles{" + "marca=" + marca + ", modelo=" + modelo + ", matricula=" + matricula + ", chofer=" + chofer + '}';
    }    
    
}
