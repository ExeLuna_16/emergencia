package emergencia;

import excepciones.ObjetoInexistenteException;
import excepciones.ObjetoNuloException;
import gestionAfiliados.Afiliado;
import gestionAfiliados.Familiar;
import gestionAsistenciaMedica.SolicitudEmergencia;
import gestionVentanas.VentanaSolicitudDeEmergencia;
import gestionDeRegistros.RegistroAfiliados;
import gestionDeRegistros.RegistroDeEmpleados;
import gestionPago.Factura;
import gestionPago.Factura.Item;
import java.time.LocalDate;
import java.util.List;
import gestionDeRegistros.RegistroFacturas;
import gestionEmpleados.Administrativo;
import gestionEmpleados.Chofer;
import gestionEmpleados.Empleado;
import gestionEmpleados.Enfermero;
import gestionEmpleados.Medico;
import gestionVentanas.Principal;
import gestionVentanas.VCargaAfiliado;
import gestionVentanas.VCargaFamiliar;
import gestionVentanas.VentanaEmpleado;
import java.util.ArrayList;

/*ACA SE TIENE QUE INSTANCIAR SOLO SISTEMA PRINCIPAL Y NADA MAS*/
public class Emergencia {
    /*static para no porque no se puede instanciar SistemaPrincipal*/
    //private static SistemaPrincipal.AdministradorDePrecios precios = SistemaPrincipal.instancia().getAdministradorDePrecios();
    //private RegistroAfiliados reg = SistemaPrincipal.instancia().getRegistroAfiliados();
    
    public static void main(String[] args) {
        
        Principal ventana = new Principal();
        ventana.setVisible(true);
    }
}

    
