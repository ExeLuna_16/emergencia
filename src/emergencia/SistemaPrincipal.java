package emergencia;

import gestionAfiliados.Afiliado;
import gestionDeRegistros.RegistroAfiliados;
import gestionDeRegistros.RegistroDeEmpleados;
import gestionDeRegistros.RegistroFacturas;
import gestionDeRegistros.RegistroSolicitudDeEmergencia;
import gestionPago.Factura;
import gestionPago.Moratoria;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;



public class SistemaPrincipal implements Moratoria{
    //ATRIBUTOS
    private static SistemaPrincipal sistema; //--> Statico
    private RegistroAfiliados registroAfiliados;
    private RegistroDeEmpleados registroEmplados;
    private RegistroSolicitudDeEmergencia solicitudes;
    private RegistroFacturas libroContable;
    private AdministradorDePrecios precios;
    
    //CONSTRUCTOR
    /*Constructor privado para que nadie pueda crear un objeto desde afuera*/
    private SistemaPrincipal() {
        this.registroAfiliados = new RegistroAfiliados();
        this.registroEmplados = new RegistroDeEmpleados();
        this.solicitudes = new RegistroSolicitudDeEmergencia();
        this.libroContable = new RegistroFacturas();
        this.precios = new AdministradorDePrecios();
    }
    
    //Patron Singelton
    /*Si no hay una instancia del sistema, la crea. De lo contrario
    es retornada la referencia existente, por lo tanto esto permite que no
    exista más de una instancia de mi sistema principal*/
    public static SistemaPrincipal instancia(){
        if(sistema == null){ 
            sistema = new SistemaPrincipal();
        }
        return sistema;
    }
    
    
    //Referencia a los repositorios
    public RegistroAfiliados getRegistroAfiliados() {
        return registroAfiliados;
    }
    public RegistroDeEmpleados getRegistroEmplados() {
        return registroEmplados;
    }
    public RegistroSolicitudDeEmergencia getSolicitudes() {
        return solicitudes;
    }
    public RegistroFacturas getLibroContable() {
        return libroContable;
    }
    
    public AdministradorDePrecios getAdministradorDePrecios(){
        return precios;
    }

    @Override
    public void controlDeMora(String dniAfiliadoBuscado){
        Afiliado aux = registroAfiliados.buscarAfiliado(dniAfiliadoBuscado);
        LocalDate fechaActual = LocalDate.now();
        Factura ultimaFactura = aux.ultimaFactura();
        if(ultimaFactura != null && ChronoUnit.MONTHS.between(ultimaFactura.getFechaEmision(), fechaActual) > 2){
            reduccionDeServicio(aux);
        } 
    }
    
    @Override //Cambia a false
    public void reduccionDeServicio(Afiliado enMora){
        enMora.setTieneServicio(false);
    }

    @Override //Cambia a true
    public void altaDeServicio(Afiliado enMora){
        enMora.setTieneServicio(true);
    }
    
    public class AdministradorDePrecios{
      
        private Double costoAfiliacion;
        private Double costoPorFamiliar;
        
        public AdministradorDePrecios(){
            this.costoAfiliacion = 1010.0;
            this.costoPorFamiliar = 111.0;
        }
        public void actualizarPrecioAfiliacion(Double precioNuevo){
            setCostoAfiliacion(precioNuevo);
        }
        public void actualizarPreciPorFamiliar(Double precioNuevo){
            setCostoPorFamiliar(precioNuevo);
        }
        //Setters y Getters
        public Double getCostoAfiliacion() {
            return costoAfiliacion;
        }
        private void setCostoAfiliacion(Double costoAfiliacion) {
            this.costoAfiliacion = costoAfiliacion;
        }
        public Double getCostoPorFamiliar() {
            return costoPorFamiliar;
        }
        private void setCostoPorFamiliar(Double costoPorFamiliar) {
            this.costoPorFamiliar = costoPorFamiliar;
        }
        
    }
    
}
