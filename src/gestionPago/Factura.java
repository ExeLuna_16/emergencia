package gestionPago;

import gestionAfiliados.Afiliado;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDate;
/**
 *
 * @author ExeLuna__
 */
public class Factura {
    //Atributos
    private LocalDate fechaEmision;
    private Afiliado clienteAfiliado;
    private String nombreEmpresa;
    private Character tipoFactura; //---> A, B , C o la que fuere.
    private Integer numeroFactura;
    private Integer cuit;
    private Boolean iva; //impuesto consultar si corresponde o no
    private List<Item> items; //Recorrer los items, acumular y sumar
    
    //Constructor
    public Factura(Character tipoFactura,Afiliado afiliado,Integer numeroFactura,Boolean iva){
        this.nombreEmpresa = "Inserte Nombre Empresa";
        this.tipoFactura = tipoFactura;
        this.clienteAfiliado = afiliado;
        this.numeroFactura = numeroFactura;
        this.fechaEmision = LocalDate.now();
        this.cuit = cuit;
        this.iva = iva;//calcular
        this.items = new ArrayList<Item>();
    }
    
    /*Agregar item*/
    public void agregarItem(Item nuevo){
        Item aux = buscarItem(nuevo.getCodigo());
        if(aux != null){
            throw new RuntimeException("El item ya existe");
        }else{
            items.add(nuevo);
        }
    }
    /*buscar item
    se usa while porque es mas eficiente que un foreach ya que el forach busca en todos los elementos
    por más de que ya lo haya encontrado.
    La instructtion negada del while indica que si lo encuentra entonces de "true" pasa a "false" razon
    por la que el ciclo termina*/
    public Item buscarItem(Integer codigoBuscado){ //Busqueda mas eficiente con while
        int i = 0;
        while(i < items.size() && !items.get(i).equals(codigoBuscado)){
            i++;
        }
        if(i < items.size()){
            return items.get(i);
        }
        //throw new RuntimeException("item no encontrado");
        return null;
    }
    
    /*Eliminar item*/
    public void eliminarItem(Integer buscado){
        Item borrar = buscarItem(buscado);
        if(borrar == null){
          throw new RuntimeException("item no encontrado"); 
        }else{
            items.remove(borrar);
        }
    }
    
    /*Modificar item*/
    public void modificarItem(Integer cambiar,Item nuevo){
        eliminarItem(cambiar);
        agregarItem(nuevo);
    }
  
    /*Agregar Iva*/
    public Double agregarIva(Item itemAux){
        Double subtotal = itemAux.totalItem();
        return subtotal * 0.21;
    }
    
    /*Total de la factura*/
    public Double totalFactura(){
        Double total = 0.0;
        for(Item aux:items){
            total = total + aux.totalItem();
            if(this.iva){ //Si es true entrara, lo que significa que se cobra iva
                 total = total + agregarIva(aux);
            }
        }
        return total;
    }
    
    //Setters y getters
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public Character getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(Character tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public Integer getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Integer numeroFactura) {
        this.numeroFactura = numeroFactura;
    }
    
    public Integer getCuit() {
        return cuit;
    }

    public void setCuit(Integer cuit) {
        this.cuit = cuit;
    }

    public LocalDate getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(LocalDate fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Afiliado getClienteAfiliado() {
        return clienteAfiliado;
    }

    public void setClienteAfiliado(Afiliado clienteAfiliado) {
        this.clienteAfiliado = clienteAfiliado;
    }
    
    
    /*Metodo equals para diferenciar facturas*/
    @Override
    public boolean equals(Object ob){
        if(this == ob){
            return true;
        }
        if(ob == null){
            return false;
        }
        return this.getNumeroFactura().equals(ob);
    }
  
    //Clase interna
    public class Item{ 
        //Atributos
        /*monto del afiliado consultar si es fijo o como hacer para actualizarlo*/
        private Double montoAfiliacion;
        private Double montoPorFamiliar;//monto porfamiliar
        private Integer cantidadFamiliares;
        private String descripcion;
        private Integer codigo;
        
        //Constructor
        public Item(Double montoAfiliacion, Double montoPorFamiliar,Integer cantidadFamiliares,String descripcion,Integer codigo) {
            this.montoAfiliacion = montoAfiliacion;
            this.montoPorFamiliar = montoPorFamiliar;
            this.cantidadFamiliares = cantidadFamiliares;
            this.descripcion = descripcion;
            this.codigo = codigo;
        }
        /*Agregar metodo total Item que me de el total de un item*/
        /*Precio por afiliar un familiar por la cantidad de familiares agregados
        más la suma de la mensualidad del afiliado y multiplicado por la cantidad de meses que quiere pagar
        el afiliado*/
        public Double totalItem(){        
            Double total = ((montoPorFamiliar * cantidadFamiliares) + montoAfiliacion);
            return total;
        }
        
        //Setters y getters
        public Double getMontoAfiliacion() {
            return montoAfiliacion;
        }

        public void setMontoAfiliacion(Double montoAfiliacion) {
            this.montoAfiliacion = montoAfiliacion;
        }

        public Double getMontoPorFamiliar() {
            return montoPorFamiliar;
        }

        public void setMontoPorFamiliar(Double montoPorFamiliar) {
            this.montoPorFamiliar = montoPorFamiliar;
        }
        
        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public Integer getCantidadFamiliares() {
            return cantidadFamiliares;
        }

        public void setCantidadFamiliares(Integer cantidadFamiliares) {
            this.cantidadFamiliares = cantidadFamiliares;
        }

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }
        
        @Override
        public boolean equals(Object ob){
            if(this == ob){
                return true;
            }
            if(ob == null){
                return false;
            }
            return this.codigo.equals(ob);
        }
        
    }
    
    
    
}
