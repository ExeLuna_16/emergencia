package gestionPago;

import gestionAfiliados.Afiliado;

/**
 *
 * @author ExeLuna__
 */
public interface Moratoria {
    
    public void reduccionDeServicio(Afiliado enMora);
    public void altaDeServicio(Afiliado enMora);
    public void controlDeMora(String dniAfiliadoBuscado);
}
    