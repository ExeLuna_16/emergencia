/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package excepciones;

/**
 *
 * @author Ordenador
 */
public class ObjetoInexistenteException extends Exception {
    public ObjetoInexistenteException(String mensaje) {
        super(mensaje);
    }
    
}
