
package excepciones;


public class ObjetoNuloException extends Exception{

    public ObjetoNuloException(String messaje) {
        super(messaje);
    }
    
}
