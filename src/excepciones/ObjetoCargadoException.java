package excepciones;

/**
 *
 * @author ExeLuna__
 */
public class ObjetoCargadoException extends Exception{
    public ObjetoCargadoException(String m){
        super(m);
    }
}
