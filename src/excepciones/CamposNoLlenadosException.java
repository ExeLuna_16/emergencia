
package excepciones;


public class CamposNoLlenadosException extends Exception{

    public CamposNoLlenadosException(String mensaje) {
        super(mensaje);
    }
    
}
